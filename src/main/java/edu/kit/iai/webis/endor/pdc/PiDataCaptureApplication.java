package edu.kit.iai.webis.endor.pdc;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

@SpringBootApplication
@EnableScheduling
public class PiDataCaptureApplication {

	/**
	 * Application entry point
	 * 
	 * @param args
	 *            CLI arguments
	 */
	public static void main(final String[] args) {
		SpringApplication.run(PiDataCaptureApplication.class, args);
	}

	/**
	 * Create GSON bean with NodeRed/Javascript compatible date format as type adapter for {@link Date} objects
	 * 
	 * @return GSON bean
	 */
	@Bean
	public Gson gson() {
		return new GsonBuilder().registerTypeAdapter(Date.class, new DateJSCompatibleSerializer()).create();
	}

	/**
	 * Javascript <code>Date.parse</code> compatible date (de-)serialization. Pattern of GSON builder could not set the locale
	 */
	private class DateJSCompatibleSerializer implements JsonSerializer<Date>, JsonDeserializer<Date> {
		private final SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);

		@Override
		public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
			if (json.isJsonPrimitive()) {
				try {
					return this.dateFormatter.parse(json.getAsString());
				} catch (final ParseException e) {
					throw new JsonParseException(e);
				}
			}
			return null;
		}

		@Override
		public JsonElement serialize(final Date src, final Type typeOfSrc, final JsonSerializationContext context) {
			return new JsonPrimitive(this.dateFormatter.format(src));
		}

	}
}
