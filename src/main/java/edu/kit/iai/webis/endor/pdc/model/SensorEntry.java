package edu.kit.iai.webis.endor.pdc.model;

import java.util.Date;

public class SensorEntry {

	public enum Type {
		temperature, humidity
	}

	private final String sensorId;
	private final Type type;
	private final Date timestamp;
	private final float value;

	public SensorEntry(final String sensorId, final Type type, final Date timestamp, final float value) {
		super();
		this.sensorId = sensorId;
		this.type = type;
		this.timestamp = timestamp;
		this.value = value;
	}

	public String getSensorId() {
		return this.sensorId;
	}

	public Type getType() {
		return this.type;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public float getValue() {
		return this.value;
	}

}
