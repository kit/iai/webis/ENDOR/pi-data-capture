package edu.kit.iai.webis.endor.pdc.beans;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.kit.iai.webis.endor.pdc.config.CaptureConfig;
import edu.kit.iai.webis.endor.pdc.sensors.DHTReader;
import edu.kit.iai.webis.endor.pdc.sensors.DS18B20Reader;
import edu.kit.iai.webis.endor.pdc.sensors.ISensorDataReader;

@Component
public class SensorReaderBean {

	private final List<ISensorDataReader> readers = new LinkedList<>();

	@Autowired
	public SensorReaderBean(final CaptureConfig captureConfig, final DHTInfoBean dhtInfo) {
		super();

		if (captureConfig.isDs18b20()) {
			this.readers.add(new DS18B20Reader());
		}
		if (captureConfig.isDht()) {
			this.readers.add(new DHTReader(dhtInfo));
		}
		if (captureConfig.isSte2()) {
			throw new NotImplementedException("STE2 sensor reader needs to be migrated");
		}
	}

	public List<ISensorDataReader> getReaders() {
		return this.readers;
	}

}
