package edu.kit.iai.webis.endor.pdc.beans;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import edu.kit.iai.webis.endor.pdc.config.CaptureConfig;

/**
 * DHT device tree overlay ID / GPIO connection information bean
 */
@Component
public class DHTInfoBean {

	/**
	 * Logger
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(DHTInfoBean.class);

	/**
	 * Map with device tree overlay ID as key and sensord ID as value
	 */
	private final Map<Integer, String> dtoIdToSensorId = new HashMap<Integer, String>();

	/**
	 * Capture configuration
	 */
	private final CaptureConfig captureConfig;

	/**
	 * Create the capture configuration
	 * 
	 * @param captureConfig
	 *            capture configuration
	 */
	public DHTInfoBean(final CaptureConfig captureConfig) {
		super();
		this.captureConfig = captureConfig;
	}

	/**
	 * Set the values of the mapping
	 * 
	 * @param newMapping
	 *            new mapping to set
	 */
	public void setValues(final Map<Integer, Integer> newMapping) {
		this.dtoIdToSensorId.clear();
		if (newMapping != null) {
			newMapping.forEach((dtoId, gpio) -> {
				final String sensorId = this.captureConfig.getDhtPins().get(gpio);
				if (sensorId != null) {
					DHTInfoBean.LOGGER.debug("Add DHT sensor (dto id {} @ GPIO {}) as {}", dtoId, gpio, sensorId);
					this.dtoIdToSensorId.put(dtoId, sensorId);
				} else {
					DHTInfoBean.LOGGER.info("Found DHT sensor in device tree overlay (dto id {} @ GPIO {}), but no matching sensor ID", dtoId, gpio);
				}
			});
		}
	}

	/**
	 * Get the map with device tree overlay ID as key and sensor ID as value
	 * 
	 * @return map DTO-ID to GPIO (BCM numbering)
	 */
	public Map<Integer, String> getDtoIdToSensorIdMap() {
		return this.dtoIdToSensorId;
	}
}
