package edu.kit.iai.webis.endor.pdc.config;

import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Configuration what shall be captured
 */
@ConfigurationProperties(prefix = "capture")
@Component
public class CaptureConfig {

	/**
	 * Active flag for DS18B20 1-wire sensors
	 */
	private boolean ds18b20;

	/**
	 * List of STE2 sensor endpoints
	 */
	private List<String> ste2;

	/**
	 * DHT sensor map (key is GPIO, value is sensor ID)
	 */
	private Map<Integer, String> dhtPins;

	/**
	 * Read timeout for <b>all</b> sensors in seconds
	 */
	private long readTimeout = 40;

	/**
	 * Write timeout for backend system communication in seconds
	 */
	private int writeTimeout = 5;

	/**
	 * Write to this HTTP endpoint
	 */
	private String writeTo;

	/**
	 * HTTP method to write with
	 */
	private String writeMethod = "PUT";

	/**
	 * Value set as authorization header
	 */
	private String writeAuth;

	/**
	 * Cache file for failed write operations. Relative paths will be resolved against home directory of user.
	 */
	private String failedWriteCacheFile = "pi-data-capture-cache.json";

	/**
	 * Check if DHT sensors shall be read
	 * 
	 * @return <code>true</code> if DHT sensors shall be read (meaning information is available), false otherwise
	 */
	public boolean isDht() {
		return this.dhtPins != null && !this.dhtPins.isEmpty();
	}

	/**
	 * Check if DS18B20 sensors shall be read
	 * 
	 * @return @return <code>true</code> if DS18B20 sensors shall be read (meaning information is available), false otherwise
	 */
	public boolean isDs18b20() {
		return this.ds18b20;
	}

	/**
	 * Set if DS18B20 sensors shall be read
	 * 
	 * @param ds18b20
	 *            read flag
	 */
	public void setDs18b20(final boolean ds18b20) {
		this.ds18b20 = ds18b20;
	}

	/**
	 * /**
	 * Check if STE2 sensors shall be read
	 * 
	 * @return @return <code>true</code> if DS18B20 sensors shall be read (meaning information is available), false otherwise
	 */
	public boolean isSte2() {
		return this.ste2 != null && !this.ste2.isEmpty();
	}

	/**
	 * Get list of STE2 sensor endpoints
	 * 
	 * @return list of STE2 sensor endpoints
	 */
	public List<String> getSte2() {
		return this.ste2;
	}

	/**
	 * Set list of STE2 sensor endpoints
	 * 
	 * @param ste2
	 *            list of STE2 sensor endpoints
	 */
	public void setSte2(final List<String> ste2) {
		this.ste2 = ste2;
	}

	/**
	 * Get the DHT pin/sensorId map
	 * <ul>
	 * <li>Key: GPIO the sensor is connected to (<code>dtoverlay</code> compatible numbering scheme &rArr; BCM numbering)</li>
	 * <li>Value: Sensor ID</li>
	 * </ul>
	 * 
	 * @return DHT pin/sensorId map
	 */
	public Map<Integer, String> getDhtPins() {
		return this.dhtPins;
	}

	/**
	 * Set the DHT pin/sensorId map
	 * 
	 * @param dhtPins
	 *            DHT pin/sensorId map to set
	 * @see #getDhtPins()
	 */
	public void setDhtPins(final Map<Integer, String> dhtPins) {
		this.dhtPins = dhtPins;
	}

	/**
	 * Set read timeout for <b>all</b> sensors in seconds
	 * 
	 * @param readTimeout
	 *            read timeout for <b>all</b> sensors in seconds
	 */
	public void setReadTimeout(final long readTimeout) {
		this.readTimeout = readTimeout;
	}

	/**
	 * Get the read timeout for <b>all</b> sensors in seconds
	 * 
	 * @return read timeout for <b>all</b> sensors in seconds
	 */
	public long getReadTimeout() {
		return this.readTimeout;
	}

	/**
	 * Set the write timeout for backend system communication in seconds
	 * 
	 * @param writeTimeout
	 *            timeout to set
	 */
	public void setWriteTimeout(final int writeTimeout) {
		this.writeTimeout = writeTimeout;
	}

	/**
	 * Get the write timeout for backend system communication in seconds
	 * 
	 * @return write timeout for backend system communication in seconds
	 */
	public int getWriteTimeout() {
		return this.writeTimeout;
	}

	/**
	 * Get the HTTP endpoint to write to
	 * 
	 * @return HTTP endpoint to write to
	 */
	public String getWriteTo() {
		return this.writeTo;
	}

	/**
	 * Set the HTTP endpoint to write to
	 * 
	 * @param writeTo
	 *            HTTP endpoint to write to to set
	 */
	public void setWriteTo(final String writeTo) {
		this.writeTo = writeTo;
	}

	/**
	 * Get the HTTP method to write with
	 * 
	 * @return HTTP method to write with
	 */
	public String getWriteMethod() {
		return this.writeMethod;
	}

	/**
	 * HTTP method to write with
	 * 
	 * @param writeMethod
	 *            HTTP method to write with to set
	 */
	public void setWriteMethod(final String writeMethod) {
		this.writeMethod = writeMethod;
	}

	/**
	 * Get the value set as authorization header
	 * 
	 * @return value set as authorization header
	 */
	public String getWriteAuth() {
		return this.writeAuth;
	}

	/**
	 * Set the value set as authorization header
	 * 
	 * @param writeAuth
	 *            value set as authorization header to set
	 */
	public void setWriteAuth(final String writeAuth) {
		this.writeAuth = writeAuth;
	}

	/**
	 * Get the cache file for failed write operations. Relative paths will be resolved against home directory of user.
	 * 
	 * @return cache file for failed write operations
	 */
	public String getFailedWriteCacheFile() {
		return this.failedWriteCacheFile;
	}

	/**
	 * Set the cache file for failed write operations. Relative paths will be resolved against home directory of user.
	 * 
	 * @param failedWriteCacheFile
	 *            cache file for failed write operations to set
	 */
	public void setFailedWriteCacheFile(final String failedWriteCacheFile) {
		this.failedWriteCacheFile = failedWriteCacheFile;
	}
}
