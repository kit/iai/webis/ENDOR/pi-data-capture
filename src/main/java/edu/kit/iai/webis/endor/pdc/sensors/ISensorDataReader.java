package edu.kit.iai.webis.endor.pdc.sensors;

import java.util.List;

import edu.kit.iai.webis.endor.pdc.model.SensorEntry;

/**
 * Common interface for all sensor readers
 */
public interface ISensorDataReader {

	/**
	 * Read all entries for sensors of this type
	 * 
	 * @return entries list
	 */
	List<SensorEntry> getEntries();
}
