package edu.kit.iai.webis.endor.pdc.sensors;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import edu.kit.iai.webis.endor.pdc.model.SensorEntry;

/**
 * Reader for DS18B20 1-wire sensors via the device tree overlay files
 */
public class DS18B20Reader implements ISensorDataReader {

	/**
	 * Parent path for all 1-wire devices
	 */
	private final static Path W1_DEVICE_PATH = Paths.get("/sys/bus/w1/devices");

	/**
	 * Get entries for all connected DS18B20 sensors
	 * 
	 * @return list of entries
	 */
	@Override
	public List<SensorEntry> getEntries() {
		try {
			return Files.list(DS18B20Reader.W1_DEVICE_PATH).filter(this::isDs18b20Family).map(this::toSensorEntry).filter(Objects::nonNull).collect(Collectors.toList());
		} catch (final IOException e) {
			return Collections.emptyList();
		}
	}

	/**
	 * Convert sensor directory to sensor entry
	 * 
	 * @param p
	 *            directory of the sensor containing the <code>w1_slave</code> file
	 * @return sensor entry
	 */
	private SensorEntry toSensorEntry(final Path p) {
		return this.toSensorEntry(p, 2);
	}

	/**
	 * Convert sensor directory to sensor entry
	 * 
	 * @param p
	 *            directory of the sensor containing the <code>w1_slave</code> file
	 * @param retry
	 *            flag if value shall be re-read if reading failed
	 * @return sensor entry
	 */
	private SensorEntry toSensorEntry(final Path p, final int retry) {
		float val = Float.NaN;
		try {
			final List<String> lines = Files.readAllLines(p.resolve("w1_slave"));
			if (lines.size() >= 2) {
				if (StringUtils.trimToEmpty(lines.get(0)).endsWith("YES")) {
					final String valLine = StringUtils.trimToEmpty(lines.get(1));
					final int idxEq = valLine.lastIndexOf('=');
					if (idxEq >= 0) {
						BigDecimal bd = BigDecimal.valueOf(Long.parseLong(valLine.substring(idxEq + 1)));
						bd = bd.movePointLeft(3);
						val = bd.floatValue();
					}
				}
			}
		} catch (final IOException e) {
		}

		if (Float.isNaN(val)) {
			return retry <= 0 ? this.toSensorEntry(p, retry - 1) : null;
		} else {
			return new SensorEntry(p.getFileName().toString(), SensorEntry.Type.temperature, new Date(), val);
		}
	}

	/**
	 * Check if directory name is sensor of DS18B20 family (checks for <code>28-</code prefix)
	 * 
	 * &#64;param p
	 *            1-wire device directory
	 * 
	 * @return <code>true</code> if sensor of DS18B20 family, <code>false</code> otherwise
	 */
	private boolean isDs18b20Family(final Path p) {
		final String s = p != null ? p.getFileName().toString() : null;
		return s != null ? s.startsWith("28-") : false;
	}

}
