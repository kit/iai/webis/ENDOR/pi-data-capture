package edu.kit.iai.webis.endor.pdc.controller;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import edu.kit.iai.webis.endor.pdc.config.CaptureConfig;
import edu.kit.iai.webis.endor.pdc.model.SensorEntry;

@Component
public class WriteController {

	/**
	 * Logger
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(WriteController.class);

	/**
	 * Request factory to parse http method just once and already set target URI
	 */
	private final Supplier<HttpEntityEnclosingRequestBase> requestFactory;

	/**
	 * GSON instance
	 */
	private final Gson gson;

	/**
	 * HTTP client
	 */
	private final CloseableHttpClient client;

	private final Path failedWritesCacheFile;

	@Autowired
	public WriteController(final Gson gson, final CaptureConfig captureConfig) throws IOException {
		super();
		this.gson = gson;

		this.client = this.createClient(captureConfig);
		this.requestFactory = this.createRequestFactory(captureConfig);
		this.failedWritesCacheFile = this.createFailedWriteCacheFilePath(captureConfig);
	}

	public void writeData(final List<SensorEntry> data) {
		if (!this.doWriteData(data)) {
			WriteController.LOGGER.info("Could not write to backend system - storing data in cache for delayed write operation.");
			this.supplyDataForDelayedWrite(data);
		}
	}

	@Scheduled(cron = "${capture.delayedInterval}")
	public void writeDelayed() {
		// all operations with cache file shall synchronize on cache file path to not interfere with each other
		synchronized (this.failedWritesCacheFile) {
			try {
				final List<String> lines = Files.readAllLines(this.failedWritesCacheFile);
				final List<SensorEntry> sensorEntries = lines.stream().filter(StringUtils::isNotBlank).map(l -> this.gson.fromJson(l, SensorEntry[].class)).flatMap(Stream::of).collect(Collectors.toList());
				if (!sensorEntries.isEmpty()) {
					WriteController.LOGGER.debug("Found {} lines with {} sensor entries in failed write cache file, attempt to re-write", lines.size(), sensorEntries.size());
				}
				if (!sensorEntries.isEmpty() && this.doWriteData(sensorEntries)) {
					WriteController.LOGGER.info("Successfully wrote {} entries from cache", sensorEntries.size());
					try (BufferedWriter bw = Files.newBufferedWriter(this.failedWritesCacheFile)) {
						WriteController.LOGGER.debug("Cache cleared", sensorEntries.size());
					} catch (final Exception e) {
						WriteController.LOGGER.debug("Cache could not be cleared", sensorEntries.size());
					}
				} else {
					WriteController.LOGGER.debug("Re-write did not succeed - keeping all entries in cache");
				}
			} catch (final IOException e) {
				WriteController.LOGGER.debug("Could not read failed entries cache.");
			}
		}
	}

	private CloseableHttpClient createClient(final CaptureConfig captureConfig) {
		final PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(100);
		cm.setDefaultMaxPerRoute(20);

		final int timeoutMs = captureConfig.getWriteTimeout() * 1000;
		final RequestConfig rc = RequestConfig.custom().setConnectTimeout(timeoutMs).setSocketTimeout(timeoutMs).setConnectionRequestTimeout(timeoutMs).build();

		return HttpClients.custom().setDefaultRequestConfig(rc).setConnectionManager(cm).build();
	}

	private Supplier<HttpEntityEnclosingRequestBase> createRequestFactory(final CaptureConfig captureConfig) {
		final Function<String, HttpEntityEnclosingRequestBase> instanceFactory;
		switch (StringUtils.upperCase(captureConfig.getWriteMethod())) {
		case "PUT":
			instanceFactory = HttpPut::new;
			break;
		case "POST":
			instanceFactory = HttpPost::new;
			break;
		default:
			WriteController.LOGGER.warn("No valid HTTP method specified {} (may be PUT, POST), using PUT as fallback", captureConfig.getWriteMethod());
			instanceFactory = HttpPut::new;
			break;
		}
		final Optional<String> authHeader = Optional.ofNullable(StringUtils.trimToNull(captureConfig.getWriteAuth()));
		return () -> {
			final HttpEntityEnclosingRequestBase req = instanceFactory.apply(captureConfig.getWriteTo());
			authHeader.ifPresent(auth -> req.setHeader("Authorization", auth));
			return req;
		};
	}

	private boolean doWriteData(final List<SensorEntry> data) {
		CloseableHttpResponse resp = null;
		try {
			final HttpEntityEnclosingRequestBase request = this.requestFactory.get();
			request.setEntity(new StringEntity(this.gson.toJson(data), ContentType.APPLICATION_JSON));
			resp = this.client.execute(request);

			final int code = resp.getStatusLine().getStatusCode();

			return code == 201 || code == 200;
		} catch (final Exception e) {
		} finally {
			HttpClientUtils.closeQuietly(resp);
		}
		return false;
	}

	private void supplyDataForDelayedWrite(final List<SensorEntry> data) {
		// all operations with cache file shall synchronize on cache file path to not interfere with each other
		synchronized (this.failedWritesCacheFile) {
			try (BufferedWriter bw = Files.newBufferedWriter(this.failedWritesCacheFile, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
				bw.newLine();
				this.gson.toJson(data, bw);
			} catch (final Exception e) {
				WriteController.LOGGER.error("Failed to append to cache file, data of this measurement will be lost!");
			}
		}
	}

	private Path createFailedWriteCacheFilePath(final CaptureConfig captureConfig) throws IOException {
		String configVal = captureConfig.getFailedWriteCacheFile();
		if (StringUtils.isBlank(configVal)) {
			WriteController.LOGGER.warn("The failed writed cache file configuration is empty, using fallback default value 'pi-data-capture-cache.json' in home directory.");
			configVal = "pi-data-capture-cache.json";
		}
		Path cachePath = Paths.get(configVal);
		if (!cachePath.isAbsolute()) {
			cachePath = Paths.get(System.getProperty("user.home")).resolve(cachePath);
		}

		final Path parentDir = cachePath.getParent();
		if (!Files.isDirectory(parentDir)) {
			WriteController.LOGGER.info("Parent directory of failed writed cache file ({}) does not exist but will be created", parentDir);
			try {
				Files.createDirectories(parentDir);
			} catch (final IOException e) {
				WriteController.LOGGER.error("Could not create parent directory of failed writed cache file ({})", parentDir);
				throw e;
			}
		}

		if (!Files.exists(cachePath)) {
			Files.createFile(cachePath);
		}
		return cachePath;

	}

}
