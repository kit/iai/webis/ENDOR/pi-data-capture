package edu.kit.iai.webis.endor.pdc.sensors;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import edu.kit.iai.webis.endor.pdc.beans.DHTInfoBean;
import edu.kit.iai.webis.endor.pdc.model.SensorEntry;

/**
 * Reader for DHT sensor data via device tree overlay
 */
public class DHTReader implements ISensorDataReader {

	/**
	 * Parent path for all 1-wire devices
	 */
	private final static Path IIO_DEVICE_PATH = Paths.get("/sys/bus/iio/devices");

	/**
	 * DHT info bean
	 */
	private final DHTInfoBean dhtInfo;

	/**
	 * Create DHT reader
	 * 
	 * @param dhtInfo
	 *            DHT info
	 */
	public DHTReader(final DHTInfoBean dhtInfo) {
		super();
		this.dhtInfo = dhtInfo;
	}

	/**
	 * Get entries for all connected DS18B20 sensors
	 * 
	 * @return list of entries
	 */
	@Override
	public List<SensorEntry> getEntries() {
		try {
			return this.dhtInfo.getDtoIdToSensorIdMap().entrySet().stream().map(this::toSensorEntry).flatMap(DHTEntries::asStream).filter(Objects::nonNull).collect(Collectors.toList());
		} catch (final Exception e) {
			return Collections.emptyList();
		}
	}

	/**
	 * Convert sensor dto/pin entry to sensor entry
	 * 
	 * @param entry
	 *            map entry with device tree overlay id as key and sensor ID as value
	 * @return sensor entry
	 */
	private DHTEntries toSensorEntry(final Entry<Integer, String> entry) {
		return this.toSensorEntry(entry.getKey(), entry.getValue(), true);
	}

	/**
	 * Convert to sensor entry
	 * 
	 * @param dtoId
	 *            device tree overlay id
	 * @param sensorId
	 *            sensor id
	 * @param retry
	 *            re-try count if reading fails
	 * @return entry with temperature and humidity
	 */
	private DHTEntries toSensorEntry(final int dtoId, final String sensorId, final boolean retry) {
		final DHTEntries dhtEntries = new DHTEntries();

		final Path basePath = DHTReader.IIO_DEVICE_PATH.resolve("iio:device" + dtoId);

		dhtEntries.temperature = this.toSensorEntry(basePath.resolve("in_temp_input"), SensorEntry.Type.temperature, sensorId, 2);
		dhtEntries.humidity = this.toSensorEntry(basePath.resolve("in_humidityrelative_input"), SensorEntry.Type.humidity, sensorId, 2);

		return dhtEntries;
	}

	/**
	 * Convert file in device tree overlay to sensor entry. Since both temperature and humidity file are in same format, same logic can be applied.
	 * 
	 * @param path
	 *            path of file
	 * @param type
	 *            sensor entry type
	 * @param sensorId
	 *            sensor id
	 * @param retry
	 *            re-try count if reading fails
	 * @return entry with temperature or humidity
	 */
	private SensorEntry toSensorEntry(final Path path, final SensorEntry.Type type, final String sensorId, final int retry) {
		try {
			BigDecimal bd = BigDecimal.valueOf(Long.parseLong((Files.readAllLines(path).get(0))));
			bd = bd.movePointLeft(3);
			return new SensorEntry(sensorId, type, new Date(), bd.floatValue());
		} catch (final Exception e) {
			// file could not be read,try again (after 2s!) if flag set
			if (retry > 0) {
				try {
					Thread.sleep(2000l);
				} catch (final InterruptedException e1) {
				}
				return this.toSensorEntry(path, type, sensorId, retry - 1);
			} else {
				return null;
			}
		}
	}

	/**
	 * Internal wrapper for temperature and humidity value
	 */
	private class DHTEntries {
		/**
		 * Entry for temperature
		 */
		SensorEntry temperature;
		/**
		 * Entry for humidity
		 */
		SensorEntry humidity;

		/**
		 * Get both entries (temperature and humidity) as stream
		 * 
		 * @return entry stream
		 */
		Stream<SensorEntry> asStream() {
			return Stream.of(this.temperature, this.humidity);
		}
	}

}
