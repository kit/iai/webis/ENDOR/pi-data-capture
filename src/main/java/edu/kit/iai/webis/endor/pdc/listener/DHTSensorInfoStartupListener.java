package edu.kit.iai.webis.endor.pdc.listener;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import edu.kit.iai.webis.endor.pdc.beans.DHTInfoBean;
import edu.kit.iai.webis.endor.pdc.config.CaptureConfig;

/**
 * Collects information about the DHT sensors at startup (mapping of IIO device ID to GPIO)
 */
@Component
public class DHTSensorInfoStartupListener implements ApplicationListener<ContextRefreshedEvent> {

	/**
	 * Logger
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(DHTSensorInfoStartupListener.class);

	/**
	 * Info bean to write data to
	 */
	private final DHTInfoBean dhtInfoBean;

	/**
	 * Capture configuration
	 */
	private final CaptureConfig captureConfig;

	/**
	 * Create the startup listener
	 * 
	 * @param captureConfig
	 *            capture configuration
	 * @param dhtInfoBean
	 *            info bean to write data to
	 */
	@Autowired
	public DHTSensorInfoStartupListener(final CaptureConfig captureConfig, final DHTInfoBean dhtInfoBean) {
		super();
		this.captureConfig = captureConfig;
		this.dhtInfoBean = dhtInfoBean;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onApplicationEvent(final ContextRefreshedEvent event) {
		if (this.captureConfig.isDht()) {
			try {
				final String iioPathStr = "/sys/bus/iio/devices/";
				final Path iioPath = Paths.get(iioPathStr);
				if (Files.exists(iioPath)) {
					final Map<Integer, Integer> dhtDevices = Files.list(iioPath).filter(this::filterDht).collect(Collectors.toMap(this::getDhtIioDeviceId, this::getDhtIioGpio));

					if (DHTSensorInfoStartupListener.LOGGER.isDebugEnabled()) {
						if (dhtDevices.isEmpty()) {
							DHTSensorInfoStartupListener.LOGGER.debug("Found no DHT devices");
						} else {
							DHTSensorInfoStartupListener.LOGGER.debug("Found {} DHT devices: {}", dhtDevices.size(), dhtDevices.entrySet().stream().map(e -> "device" + e.getKey() + " @ GPIO" + e.getValue()).collect(Collectors.joining(", ")));
						}
					}

					this.dhtInfoBean.setValues(dhtDevices);
				} else {
					DHTSensorInfoStartupListener.LOGGER.error("IIO device tree overlay directory {} does not exist", iioPathStr);
				}
			} catch (final Exception e) {
				DHTSensorInfoStartupListener.LOGGER.error("Error while collecting IIO device id/GPIO information", e);
			}
		}
	}

	/**
	 * Filter if directory is (most probably) a DHT11/22 IIO device
	 * 
	 * @param p
	 *            path to device in device tree overlay
	 * @return <code>true</code> if directory is (most probably) a DHT11/22 IIO device
	 */
	private boolean filterDht(final Path p) {
		return Files.exists(p.resolve("in_temp_input")) && Files.exists(p.resolve("in_humidityrelative_input"));
	}

	/**
	 * Get the DHT11/22 IIO device tree overlay ID for path
	 * 
	 * @param p
	 *            path to get id from
	 * @return ID (or -1 on errors)
	 */
	private Integer getDhtIioDeviceId(final Path p) {
		try {
			return new Integer(p.getFileName().toString().substring(10));
		} catch (final Exception e) {
			DHTSensorInfoStartupListener.LOGGER.error("Could not get IIO device ID for DHT device tree overlay directory {}", p);
			return -1;
		}
	}

	/**
	 * Get the DHT11/22 IIO GPIO for path
	 * 
	 * @param p
	 *            path to get GPIO information from
	 * @return GPIO number in numbering scheme used (or -1 on errors)
	 */
	private Integer getDhtIioGpio(final Path p) {
		try {
			final byte[] gpioInfo = Files.readAllBytes(p.resolve("of_node/gpios"));
			return new Integer(gpioInfo[7]); // byte 8 (index 7) is GPIO number
		} catch (final Exception e) {
			DHTSensorInfoStartupListener.LOGGER.error("Could not get GPIO for DHT device tree overlay directory {}", p);
			return -1;
		}
	}

}
