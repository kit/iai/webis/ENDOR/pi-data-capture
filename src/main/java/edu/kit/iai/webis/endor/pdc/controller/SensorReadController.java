package edu.kit.iai.webis.endor.pdc.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import edu.kit.iai.webis.endor.pdc.beans.SensorReaderBean;
import edu.kit.iai.webis.endor.pdc.config.CaptureConfig;
import edu.kit.iai.webis.endor.pdc.model.SensorEntry;

@Component
public class SensorReadController {

	/**
	 * Logger for import application
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(SensorReadController.class);

	/**
	 * Executor for the reading, single threaded as reading the sensors simultaneously may result in collisions.
	 */
	private final ExecutorService readExecutor = Executors.newSingleThreadExecutor();

	/**
	 * Sensor read bean
	 */
	private final SensorReaderBean sensorReaderBean;

	/**
	 * Capture configuration
	 */
	private final CaptureConfig captureConfig;

	/**
	 * Write controller
	 */
	private final WriteController writeController;

	@Autowired
	public SensorReadController(final CaptureConfig captureConfig, final SensorReaderBean sensorReaderBean, final WriteController writeController) {
		super();
		this.sensorReaderBean = sensorReaderBean;
		this.captureConfig = captureConfig;
		this.writeController = writeController;
	}

	@Scheduled(cron = "*/60 * * * * *")
	public void captureSensorData() {
		SensorReadController.LOGGER.debug("Start reading sensor data " + this.sensorReaderBean);
		final Future<List<SensorEntry>> entriesFuture = this.readExecutor.submit(this::readAll);

		try {
			final List<SensorEntry> entries = entriesFuture.get(this.captureConfig.getReadTimeout(), TimeUnit.SECONDS);
			this.writeController.writeData(entries);
		} catch (ExecutionException | TimeoutException | InterruptedException e) {
			entriesFuture.cancel(true);
			SensorReadController.LOGGER.debug("Failed to read all sensors before timeout", e);
		}
	}

	private List<SensorEntry> readAll() {
		final List<SensorEntry> entries = new LinkedList<SensorEntry>();
		this.sensorReaderBean.getReaders().forEach(r -> entries.addAll(r.getEntries()));
		return entries;
	}

}
