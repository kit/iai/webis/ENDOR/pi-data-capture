# ENDOR (ENergy Data of Offices and Residential buildings) Project

## Raspberry Pi Data Capture Application

This software gets data from connected DS18B20 1-wire and DHT11/22 sensors and sends them to HTTP data sink.

If the backend system cannot be reached when sensors data is being read, the data will be cached and re-written later on. Cache is stored on the file system, so it will survive re-starts of the application and/or operating system.

Copyright (c) 2018-2020 Karlsruhe Institute of Technology (KIT), Institute for Automation and Applied Informatics (IAI)

For license information see LICENSE.txt

### Preconditons
This software relies on the precense of device tree overlays for both DS18B20 and DHT11/20 sensors.

In order to be able to read multiple DHT11/20 sensors at one Raspberry Pi **check that you have a recent version of the device tree overlay**!

### Configuration
Given a configuration where GPIO 4 would be used for DS18B20 1-wire bus and GPIO20 and 21 used for DHT sensors, your `/boot/config.txt` on the Rapsberry Pi should contain something like this:

```
dtoverlay=w1-gpio,gpiopin=4
dtoverlay=dht11,gpiopin=20
dtoverlay=dht11,gpiopin=21
```

This configuration will be used in examples later on.

*Note on GPIO numbers*: All values in BCM numbering scheme

### Usage

#### Configuration Parameter
The software requires various configuration parameters like sensors to use and how to access the backend HTTP data sink.
All parameters may be provided in any way supported by [Spring Configuration](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config).

- `capture.ds18b20`: Flag to indicate if DS18B20 1-wire sensors shall be read or not (possible values: `true|false`, defaults to `false`)
- `capture.dhtPins`: Map with GPIO Pins as key and the sensor ID as value to indicate what DHT11/22 sensors shall be read. 
- `capture.interval`: Interval to schedule sensor read cycles in cron*-like* syntax. Defaults to `0 * * * * *` (1 time per minute at second 0).
- `capture.delayedInterval`: Interval of delayed write attempts if cache contains data that could not be written. Also in cron*-like* syntax and defaults to `30 0 * * * *` (1 time per hour at minute 0 and second 30).
- `capture.readTimeout`: Timeout (in seconds) for all sensors to complete a reading cycle (should be less than your scheduled read interval). Defaults to 40 seconds.
- `capture.writeTimeout`: Timeout (in seconds) for write operations in order to prevent blocking/freezed connections. Defaults to 5 seconds.
- `capture.writeTo`: The HTTP(s) endpoint to write to
- `capture.writeMethod`: The HTTP method to use, defaults to `PUT`
- `capture.writeAuth`: Optional HTTP authorization header value. Only applied if present (for an example, see below)
- `capture.failedWriteCacheFile`: Cache file to store values that could not be written so far, defaults to `pi-data-capture-cache.json` in the home directory of executing user.
  
In order to work properly at least one sensor must be configured as well as a backend system.
If no backend system is specified, all data will be written to cache file due to write failures.
  

Further documentation for Spring cron-like syntax for scheduled tasks can be found here:
- [https://spring.io/guides/gs/scheduling-tasks/](https://spring.io/guides/gs/scheduling-tasks/)
- [https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/scheduling/support/CronSequenceGenerator.html](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/scheduling/support/CronSequenceGenerator.html)

Simple summary: just like unix cron, but with additional second field.

#### Example YML configuration file

The configuration for Spring applications could be provided via an external YML configuration file.
For this application it could look like this for the sensor configuration described above:

```yml
capture:
  # read values from all connected DS18B20 1-wire sensors
  ds18b20: true
  # define which GPIOs are connected to which DHT11/20 sensors 
  dhtPins:
    20: 'DHTSensorA'
    21: 'DHTSensorB'
  # your endpoint, e.g. defined in NodeRed
  writeTo: 'https://example.org/my-raspi-endpoint'
  # HTTP method to use for write operations
  writeMethod: 'PUT'
  # Assuming basic authentication with user "foo" and password "bar" ("foo:bar" in base64 encoding)
  writeAuth: 'Basic Zm9vOmJhcg=='
```

#### Example invocation
With the following command the config YML file described above would be used as configuration input for the data capture software:

```bash
java -jar pi-data-capture.jar --spring.config.location=file:./endor-pi-data-capture-config.yml,classpath:/application.properties
```
